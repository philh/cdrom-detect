# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Turkish messages for debian-installer.
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Recai Oktaş <roktas@omu.edu.tr>, 2004, 2005, 2008.
# Osman Yüksel <yuxel@sonsuzdongu.com>, 2004.
# Özgür Murat Homurlu <ozgurmurat@gmx.net>, 2004.
# Halil Demirezen <halild@bilmuh.ege.edu.tr>, 2004.
# Murat Demirten <murat@debian.org>, 2004.
# Mert Dirik <mertdirik@gmail.com>, 2008-2012, 2014.
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001.
# (translations from drakfw)
# Fatih Demir <kabalak@gmx.net>, 2000.
# Free Software Foundation, Inc., 2000,2004
# Kemal Yilmaz <kyilmaz@uekae.tubitak.gov.tr>, 2001.
# Mert Dirik <mertdirik@gmail.com>, 2008, 2014.
# Nilgün Belma Bugüner <nilgun@fide.org>, 2001.
# Recai Oktaş <roktas@omu.edu.tr>, 2004.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Ömer Fadıl USTA <omer_fad@hotmail.com>, 1999.
# Fatih Altun <fatih.altun@pardus.org.tr>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2021-06-12 23:25+0300\n"
"Last-Translator: Fatih Altun <fatih.altun@pardus.org.tr>\n"
"Language-Team: Debian L10N Turkish\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid "Load drivers from removable media?"
msgstr "Taşınabilir ortamdan sürücü yükle"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"CD-ROM sürücülerini taşınabilir ortamdan, örneğin bir disket sürücüden, "
"yüklemeniz gerekebilir. Hâlihazırda böyle bir ortamınız varsa bunu "
"bilgisayarınıza takın ve devam edin. Aksi takdirde CD-ROM modüllerini elle "
"seçmeniz için size olanak tanınacaktır."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Kurulum ortamını bulmak amacıyla donanım algılanıyor"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "Manually select a module and device for installation media?"
msgstr "Elle bir CD-ROM modülü ve aygıtı seçmek ister misiniz?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "Herhangi bir CD-ROM sürücü belirlenemedi."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"CD-ROM sürücünüz eski bir Mitsumi veya başka bir IDE, SCSI olmayan CD-ROM "
"sürücü olabilir. Bu durumda kullanılacak modülü ve aygıtı siz seçmelisiniz. "
"Eğer hangi modülün ve aygıtın gerekli olduğunu bilmiyorsanız konuyla ilgili "
"belgeleri inceleyin veya CD-ROM'dan kurmak yerine ağdan kurulum yapmayı "
"deneyin."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid "Retry mounting installation media?"
msgstr "Uzaktan kurulum parolası:"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Kurulum CD'niz bağlanamıyor. Muhtemelen sürücüde CD yok. Eğer öyleyse CD'yi "
"yerleştirip tekrar deneyin."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid "Module needed for accessing the installation media:"
msgstr "CD-ROM'a erişim için gerekli modül:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Otomatik donanım tanıması işleminde bir CD-ROM sürücü bulunamadı. Eğer "
"alışılmışın dışında özelliklere sahip bir CD-ROM sürücünüz varsa (IDE veya "
"SCSI olmayan türde) gerekli modülü yüklemeyi deneyebilirsiniz."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid "Device file for accessing the installation media:"
msgstr "CD-ROM'a erişim için aygıt dosyası:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Lütfen CD-ROM sürücünüze ulaşmak için gerekli aygıt dosyasını girin. "
"Standart türde olmayan CD-ROM sürücüler standart olmayan aygıt dosyaları "
"kullanır (/dev/mcdx gibi)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Var olan aygıtları görmek için ikinci terminale geçerek (ALT+F2) \"ls /dev\" "
"komutunu kullanabilirsiniz. ALT+F1 ile buraya geri dönebilirsiniz."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Kurulum medyası taranıyor"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "${DIR} taranıyor..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid "Installation media detected"
msgstr "UNetbootin ortamı algılandı"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"CD-ROM'un otomatik tanınması başarıyla sonuçlandı. \"${cdname}\" adlı bir CD "
"bulundu. Şimdi kuruluma devam edilecek."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "UNetbootin ortamı algılandı"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Görünen o ki kullanmakta olduğunuz kurulum ortamı UNetbootin kullanılarak "
"oluşturulmuş. UNetbootin ile oluşturulan ortamların ortaya çıkardığı "
"sorunlardan ötürü kullanıcılarından sık sık şikâyetler alınmaktadır. Bu "
"nedenle, bu ortamdan yaptığınız kurulumda sorunlar yaşarsanız lütfen "
"yaşadığınız sorunları bildirmeden önce UNetbootin kullanmadan oluşturulmuş "
"bir ortam ile tekrar kurulum yapmayı deneyin."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Kurulum rehberi bir USB kurulum ortamının UNetbootin kullanılmadan doğrudan "
"nasıl oluşturulacağını anlatan bir bölüm içermektedir."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Incorrect installation media detected"
msgstr "UNetbootin ortamı algılandı"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "The detected media cannot be used for installation."
msgstr "CD-ROM sürücüde kurulumda kullanılamayacak bir CD mevcut."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Please provide suitable media to continue with the installation."
msgstr "Lütfen kuruluma devam etmek için uygun bir CD yerleştirin."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "\"Release\" dosyası okunurken hata"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"CD-ROM'un geçerli bir 'Release' dosyası içermediği veya bu dosyanın doğru "
"şekilde okunamadığı görünüyor."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"CD-ROM'un algılanması işlemini tekrar edebilirsiniz. Fakat bu teşebbüsler "
"başarılı olsa bile kurulumun gelecek aşamalarında sorunlar yaşayabilirsiniz."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Kurulum medyası kaldırılıyor/çıkarılıyor... "

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
#, fuzzy
msgid "Detect and mount installation media"
msgstr "\"exit\" komutu ile kurulum menüsüne dönebilirsiniz."
